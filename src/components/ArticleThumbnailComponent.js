/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import Moment from 'react-moment';

import { BaseComponent } from './_base-component';
import './ArticleThumbnailComponent.scss';
import { Link } from 'react-router-dom';
import { Routes } from '../constantes/routes';
import { ArticleService } from '../services/article.service';


export class ArticleThumbnailComponent extends BaseComponent {
    article;
    constructor() {
        super();
        this.state = {
            admin: false
        }
        this.deletetArticle = this.deletetArticle.bind(this);
    }

    isAdmin() {
        let adminHtml = [];
        console.log(this.state.admin)

        if (this.state.admin == "true") {
            adminHtml.push(<div className="btn-admin">
                {/* <button className="btn btn-primary btn-reserve">Modifier</button> */}
                <Link className="btn btn-primary" to={{ pathname: Routes.AdminListArticles + '/' + this.article.id }}>Modifier</Link>
                <button className="btn btn-danger btn-reserve" onClick={this.deletetArticle}>Supprimer</button>
            </div>);
        }

        this.setState({ adminHtml: adminHtml });
    }

    componentDidMount() {
        console.log(this.props.admin);
        this.setState({ admin: this.props.admin }, this.isAdmin);

    }

    async deletetArticle() {
        await ArticleService.deleteArticle(this.article.id);
        window.location.reload(false);
    }

    render() {
        this.article = this.props.article;
        console.log("\n \n ~ file: ArticleThumbnailComponent.js ~ line 15 ~ ArticleThumbnailComponent ~ render ~ this.props", this.props)
        if (!this.article) {
            this.article = {
                "id": 5,
                "title": "Test titre article",
                "picture": 'https://img.huffingtonpost.com/asset/5f1a00b8270000b10fe674d7.jpeg?cache=vM0RbxN9YX&ops=scalefit_630_noupscale',
                "content": 'content article test',
                "publicationDate": "2021-05-22T20:00:00+02:00",
                "creationDate": "2021-05-22T20:00:00+02:00",
            };
        }
        return (
            <div className="card-container">
                <div className="card card--concert shadow">
                    <div className="row g-0">
                        <div className="col-md-12 card-header-img">
                            <img src={this.article.picture} alt="" />
                        </div>
                        <div className="col-md-12 d-flex justify-content-center">
                            <div className="card-body">
                                <h5 className="card-title">{this.article.title}</h5>
                                <p className="card-text">
                                    <Moment format="D MMMM YYYY à HH:mm" locale="fr">{this.article.creationDate}</Moment></p>

                            </div>
                            <Link to={{
                                pathname: Routes.DetailArticle + '/' + this.article.id
                            }} onClick={this.handleClick}>
                                <span className="fullLink"></span>
                            </Link>
                        </div>

                    </div>
                </div>
                {this.state.adminHtml}

            </div>
        );
    }
}
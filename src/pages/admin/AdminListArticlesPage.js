import './AdminListArticlesPage.scss';
import { Link } from "react-router-dom";
import { BasePage } from '../_base-page';
import { ArticleThumbnailComponent } from '../../components/ArticleThumbnailComponent';
import React, { useState } from "react";
import { ArticleService } from '../../services/article.service';
import { Routes } from '../../constantes/routes';




class AdminListArticlesPage extends BasePage {

    constructor() {
        super();

        this.state = {

            articles: [],
            filterLocation: { 'location': 'all' },
            filterMusicCategory: { 'musicCategory': 'all' },
            filterRange: {},
        };
    }




    async getArticles() {

        const order = [];
        let articlesHtml = [];
        let resultArticle = await ArticleService.getAllArticles();
        console.log(resultArticle);
        if (resultArticle.status !== 200) {
            console.log('erreur : ');
        } else {
            for (const data of resultArticle.data) {

                console.log(data);
                articlesHtml.push(<ArticleThumbnailComponent article={data} admin="true" ></ArticleThumbnailComponent>);
            }
            this.setState({ articles: articlesHtml });
        }
    }


    async componentDidMount() {
        //Affichage de la liste des articles
        await this.getArticles();
    }
    render() {
        return (
            <div>

                <div className="container">

                    <div className="main-title">
                        <h1>Tous les articles</h1>
                    </div>
                    <div >
                        <div><p><a className="btn btn-primary" href={Routes.AdminListArticles + '/new'}>Ajouter un article</a></p></div>
                        {this.state.articles}
                    </div>
                </div >
            </div >

        );
    }
}

export default AdminListArticlesPage;
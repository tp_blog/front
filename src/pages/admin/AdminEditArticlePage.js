import './AdminListArticlesPage.scss';
import { Link, Redirect } from "react-router-dom";
import { BasePage } from '../_base-page';
import { ArticleThumbnailComponent } from '../../components/ArticleThumbnailComponent';
import React, { useState } from "react";
import { ArticleService } from '../../services/article.service';
import { Routes } from '../../constantes/routes';




class AdminEditArticlesPage extends BasePage {
    articleId;
    constructor() {
        super();

        this.state = {

            article: {},

            redirect: null
        };
        this.update = this.update.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }




    async getArticles() {

        const order = [];
        let articlesHtml = [];
        let resultArticle = await ArticleService.getAllArticles();
        console.log(resultArticle);
        if (resultArticle.status !== 200) {
            console.log('erreur : ');
        } else {
            for (const data of resultArticle.data) {

                console.log(data);
                articlesHtml.push(<ArticleThumbnailComponent article={data} admin="true" ></ArticleThumbnailComponent>);
            }
            this.setState({ articles: articlesHtml });
        }
    }


    async componentDidMount() {
        //Affichage de la liste des articles
        if (this.articleId !== "new") {
            let article = await ArticleService.getArticle(this.articleId);

            this.setState({ article: article.data });

        }
        await this.getArticles();
    }
    handleChange(e) {
        const name = e.target.name;
        let article = this.state.article;
        article[name] = e.target.value;
        this.setState({
            article: article
        })
    }
    async update(e) {
        e.preventDefault();
        if (this.articleId !== "new") {
            const response = await ArticleService.modifyArticle(this.articleId, this.state.article);
        }
        else {
            const response = await ArticleService.createArticle(this.state.article);
        }

        const route = "/admin/articles"
        this.setState({ redirect: route });
    }
    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        const { match: { params } } = this.props;
        this.articleId = params.id;
        return (
            <div>

                <div className="container">

                    <form className="container register-page__form" onSubmit={this.update}>
                        <div>
                            <div>
                                <label>
                                    Titre de l'article *
                                </label>
                                <input name="title" type="text" value={this.state.article?.title} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Image
                                </label>
                                <input name="picture" type="text" value={this.state.article?.picture} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Contenu de l'article *
                                </label>
                                <textarea name="content" value={this.state.article?.content} onChange={this.handleChange} cols="100" rows="10"></textarea>
                            </div>
                            <div>
                                <a className="btn btn-secondary" onClick={() => this.handleClick(undefined)}>Annuler</a>
                                <button className="btn btn-primary" type="submit">Valider</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div >

        );
    }
}

export default AdminEditArticlesPage;
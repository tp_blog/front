import './HomePage.scss';
import { Link } from "react-router-dom";
import { BasePage } from '../_base-page';
import { ArticleThumbnailComponent } from '../../components/ArticleThumbnailComponent';
import React, { useState } from "react";
import { Api } from '../../constantes/api';
import { ArticleService } from '../../services/article.service';
import { Routes } from '../../constantes/routes';



class HomePage extends BasePage {

    constructor() {
        super();

        this.state = {

            articles: [],

        };
    }




    async getArticles() {

        const order = [];
        let articlesHtml = [];
        let resultArticle = await ArticleService.getAllArticles();
        console.log(resultArticle);
        if (resultArticle.status !== 200) {
            console.log('erreur : ');
        } else {
            for (const data of resultArticle.data) {

                console.log(data);
                articlesHtml.push(<ArticleThumbnailComponent article={data} admin="false" ></ArticleThumbnailComponent>);
            }
            this.setState({ articles: articlesHtml });
        }
    }


    async componentDidMount() {
        //Affichage de la liste des articles
        await this.getArticles();
    }
    render() {
        return (
            <div>

                <div className="programmationPage container">

                    <div className="main-title">
                        <p><a className="btn btn-primary" href={Routes.AdminListArticles}>Admin</a></p>
                        <h1>Tous les articles</h1>
                    </div>
                    <div className="programmation-content row">
                        {this.state.articles}
                    </div>
                </div>
            </div>

        );
    }
}

export default HomePage;
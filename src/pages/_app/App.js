import './App.scss';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Routes } from '../../constantes/routes';
import HomePage from '../home/HomePage';
import AdminListArticlesPage from '../admin/AdminListArticlesPage'
import AdminEditArticlePage from '../admin/AdminEditArticlePage'
import DetailArticlePage from '../detail-article/DetailArticlePage'
function App() {
  return (
    <Router>
      <Switch className="container">
        {/* <Route path={Routes.DetailConcert + '/:id'} component={DetailConcertPage} />
      <Route path={Routes.AdminListConcerts + '/:id'} component={AdminEditConcertPage} />
      <Route path={Routes.AdminListConcerts} component={AdminListConcertsPage} /> */}
        <Route path={Routes.AdminListArticles + '/:id'} component={AdminEditArticlePage} />
        <Route path={Routes.DetailArticle + '/:id'} component={DetailArticlePage} />
        <Route path={Routes.AdminListArticles} component={AdminListArticlesPage} />
        {/* Last page */}
        <Route path={Routes.Home} component={HomePage} />
      </Switch>
    </Router>
  );
}

export default App;

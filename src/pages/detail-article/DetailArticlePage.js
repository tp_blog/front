import './DetailArticlePage.scss';
import { Link } from "react-router-dom";
import { BasePage } from '../_base-page';
import { ArticleThumbnailComponent } from '../../components/ArticleThumbnailComponent';
import React, { useState } from "react";
import { Api } from '../../constantes/api';
import { ArticleService } from '../../services/article.service';
import { Routes } from '../../constantes/routes';
import Moment from 'react-moment';


class DetailArticlePage extends BasePage {

    articleId;
    constructor() {
        super();

        this.state = {
            articles: [],
            article: undefined,

        };
    }






    async componentDidMount() {
        const articleResponse = await ArticleService.getArticle(this.articleId);
        if (articleResponse?.data) {
            this.setState({ article: articleResponse.data });
        }

    }
    render() {
        const { match: { params } } = this.props;
        this.articleId = params.id;
        return (
            <div>
                <div className="DetailConcert">
                    <div className="header row my-5 container-fluid">
                        <div className="picture col-12 col-md-4 offset-0 offset-md-2" style={{ backgroundImage: 'url(' + this.state.article?.picture + ')' }}></div>
                        <div className="concert-info col-12 col-md-6">
                            <p>{this.state.article?.title}</p>

                            <p>
                                <Moment format="D MMMM YYYY à HH:mm" locale="fr">{this.state.article?.publicationDate}</Moment>
                            </p>
                            <p>
                                <Moment format="D MMMM YYYY à HH:mm" locale="fr">{this.state.article?.creationDate}</Moment>
                            </p>
                        </div>
                        <div className="content-article">
                            <div>{this.state.article?.content}</div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default DetailArticlePage;
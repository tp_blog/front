import axios from 'axios';
import { Api } from '../constantes/api';

export class ArticleService {
    static async getAllArticles() {
        let url = Api.Url + '/articles';
        return await axios.get(url);
    }

    static async getArticle(id) {
        return await axios.get(Api.Url + '/articles/' + id);
    }


    static async createArticle(article) {
        let url = Api.Url + '/articles';
        article.creationDate = new Date();
        article.publicationDate = new Date();

        return await axios.post(url, article, {
            headers: {
                'Accept': 'application/json',
            },
        });
    }

    static async modifyArticle(id, article) {
        let url = Api.Url + '/articles/' + id;

        return await axios.put(url, article, {
            headers: {
                'Accept': 'application/json',
            },
        });
    }

    static async deleteArticle(id) {
        return await axios.delete(Api.Url + '/articles/' + id);
    }
}